# sb-terminal

An extension for Visual Studio Code that wraps the in-game scripting terminal in the Bleu Pigs script builder into a Visual Studio Code terminal. You can execute scripts and save them directly from the editor.

**NOTE:** At the time of writing, this extension REQUIRES the insider build of vscode to render a terminal. It will not work without.

## Using

To establish a connection with the script builder, execute the command from the Command Palette (Ctrl+Shift+P) called "Connect to SB". A notification will show in the bottom right with a clickable button to authorize ingame. Simply click the button and paste the copied code ingame to link the terminal.

### Running code

You can right-click your source code and choose ways to run it, or run one of the following commands from the Command Palette:

* Run File as Server Script
* Run File as LocalScript
* Save File as SB Script

## Installing

As of right now, this extension is unavailable in the Visual Studio Code Marketplace, as it uses its proposed API. Therefore, you will have to install the extension as such.

1. Clone the repository into a folder
2. If you have not already, install `vsce` by running `npm install -g vsce`
3. `cd sb-terminal/`, and execute `vsce publish`. This will export a `.vsix` file you can install.

## Contributing

Feel free to contribute. This is my first project using TypeScript, so there may be bad practices.