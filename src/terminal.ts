import * as vscode from "vscode";

function getTimestamp() {
	const date = new Date();
	const h = date.getHours();
	const m = date.getMinutes();
	const s = date.getSeconds();

	return `${h < 10 ? "0" + h : h}:${m < 10 ? "0" + m : m}:${s < 10 ? "0" + s : s}`;
}

const prefixes = <any>{
    "print": "\x1b[1;37m> ",
    "stack": "\x1b[1;34m%TS - ",

    "run": "\x1b[0;34m%TS - ",

    "error": "\x1b[1;31m%TS - ",
    "warn": "\x1b[0;33m%TS - ",

    "notice": "\x1b[0;32m%TS - ",
    "cmd": "\x1b[1;32m%TS - ",
    "get": "\x1b[1;32m%TS - Got "
};

export class SbTerminal {
    shell: any;

    write(message: string) {
        this.shell.write(message);
    }

    add(type: string, text: string) {
        this.write(prefixes[type].replace("%TS", getTimestamp()) + text + "\r\n");
    }

    show() {
        this.shell.terminal.show();
    }

    constructor() {
        this.shell = (<any>vscode.window).createTerminalRenderer("SB Terminal");
    }
}