'use strict';

import * as vscode from 'vscode';
import * as clipboard from 'copy-paste';
import { SbWebSocket } from './client';
import { SbTerminal } from './terminal';

export function activate(context: vscode.ExtensionContext) {
    let sb: SbWebSocket;
    let terminal = new SbTerminal();

    const info = vscode.window.showInformationMessage;
    const error = vscode.window.showErrorMessage;

    function executeFileAsScript(prefix: string) {
        const editor = vscode.window.activeTextEditor;
        if (editor !== undefined) {
            const document = editor.document;
            document.save();
            sb.sendCommand(prefix + document.getText());
        }
    }

    vscode.commands.registerCommand("sbTerminal.fileScript", () => {
        executeFileAsScript("c/");
    });
    vscode.commands.registerCommand("sbTerminal.fileLocalScript", () => {
        executeFileAsScript("l/");
    });
    vscode.commands.registerCommand("sbTerminal.fileSaveScript", () => {
        if (vscode.window.activeTextEditor === undefined) { return; }
        vscode.window.showInputBox({ prompt: "Enter script name" }).then((name: string | undefined) => {
            if (name !== undefined) {
                executeFileAsScript(`creates/${name}/`);
            }
        });
    });

    vscode.commands.registerCommand("sbTerminal.sendCommand", () => {
        vscode.window.showInputBox({ prompt: "Enter a command", placeHolder: "get/clean" }).then((command: string | undefined) => {
            if (command !== undefined) {
                sb.sendCommand(command);
            }
        });
    });

    vscode.commands.registerCommand("sbTerminal.disconnect", () => {
        if (sb === undefined || !sb.connected) {
            error("Not connnected.", "Connect").then(() => vscode.commands.executeCommand("sbTerminal.connect"));
            return;
        }

        sb.ws.close();
        sb.connected = false;
        info("Disconnected from SB.");
    });

    vscode.commands.registerCommand('sbTerminal.connect', () => {
        if (sb !== undefined && sb.connected) {
            error("Already connected.");
            return;
        }

        sb = new SbWebSocket("wss://scriptbuilder.antiboomz.com/ws/");
        sb.ws.on("message", (message) => {
            const response = sb.decode(message.toString());
            const data = response.data;

            switch (response.command) {
                case "verify":
                    info(`Please authorize with code "${data}."`, `authorize/${data}`).then((selection) => {
                        clipboard.copy(selection);
                    });
                    break;
                case "verified":
                    info("Verified successfully.");
                    terminal.show();
                    break;
                case "output":
                    const content = JSON.parse(data);
                    for (const lineData of content) {
                        const type = lineData[0];
                        const line = lineData[1];

                        if (type === "stack") {
                            line.split("\n").forEach((l: string) => terminal.add(type, l));
                        } else {
                            terminal.add(type, line);
                        }
                    }
                    break;
                default:
                    console.log(`Unknown command: ${response.command}\n${response.data}`);
            }
        });
        sb.ws.on("open", () => {
            sb.send("header", JSON.stringify({ "version": 2 }));
        });
        sb.ws.on("close", (code) => {
            switch(code) {
                case 1000:
                    error("Disconnected from the server.");
                    break;
                case 1006:
                    error("Client is old. Please reconnect.");
                    break;
                case 4000:
                    error("Lost connection to the game server.");
                    break;
                case 4002:
                    error("User left the game server.");
                    break;
                default:
                    error(`Unexpected connection error occured. Code: ${code}`);
            }
            sb.connected = false;
        });

    });
}

export function deactivate() {
    
}