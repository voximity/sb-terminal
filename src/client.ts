import * as WebSocket from "ws";
import { TextEncoder, TextDecoder } from "util";

export class SbWebSocket {
    ws: WebSocket;
    connected: boolean = false;

    textEncoder: TextEncoder = new TextEncoder();
    textDecoder: TextDecoder = new TextDecoder("utf8");

    bufferToString(buffer: Uint8Array) {
        return this.textDecoder.decode(buffer);
    }
    stringToBuffer(string: string) {
        return this.textEncoder.encode(string);
    }

    sendCommand(command: string) {
        this.send("cmd", command);
    }
    send(commandString: string, dataString: string) {
        const cmd = this.stringToBuffer(commandString);
        const data = this.stringToBuffer(dataString);
        const buffer = new Uint8Array(1 + cmd.length + data.length);

        buffer[0] = cmd.length - 1;
        buffer.set(cmd, 1);
        buffer.set(data, 1 + cmd.length);

        this.ws.send(buffer);
    }
    decode(event: string) {
        const buffer = new Uint8Array(this.stringToBuffer(event));
        const commandLength = buffer[0] + 1;
        const command = this.bufferToString(buffer.subarray(1, commandLength + 1));
        const data = this.bufferToString(buffer.subarray(commandLength + 1));
        
        return {"command": command, "data": data};
    }

    constructor(url: string) {
        this.ws = new WebSocket(url);
        this.connected = true;
    }
}